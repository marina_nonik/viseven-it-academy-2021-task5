class User{
    create(type){
        if (type === 'admin')
        return new Admin ("Dmytro", "Yummy", "dmytro@test.com", 1995);
        if (type === 'moderator')
        return new Moderator ("Alex", "Morti", "alex@test.com", 1982);
        if (type === 'client')
        return new Client ("John", "Smith", "john@test.com", 1976);
        if (type === 'guest')
        return new Guest ("Robert", "Merlo", "robert@test.com", 1999)

    }
    constructor(name, surname, email, yearOfBirth){
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.yearOfBirth = yearOfBirth;
    }
}

export class Guest extends User{
    read(){
        return (`I'm ${this.name}. I can read.`);
    };
}

export class Client extends Guest{
    write(){
        return (`I'm ${this.name}. I can write.`);
    };
} 

export class Moderator extends Client{
    update(){
        return (`I'm ${this.name}. I can update.`);
    };
}

export class Admin extends Moderator{
    remove(){
        return (`I'm ${this.name}. I can remove.`);
    }
}

const factory = new User();


const admin = factory.create('admin');
const moderator = factory.create('moderator');
const client = factory.create('client');
const guest = factory.create('guest');

console.log(guest.read());

console.log(client.read());
console.log(client.write());

console.log(moderator.read());
console.log(moderator.write());
console.log(moderator.update());

console.log(admin.read());
console.log(admin.write());
console.log(admin.update());
console.log(admin.remove());




