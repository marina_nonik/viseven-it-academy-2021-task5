class User{
    create(type){
        if (type === 'admin')
        return new Admin ("Dmytro", "Yummy", "dmytro@test.com", 1995);
        if (type === 'moderator')
        return new Moderator ("Alex", "Morti", "alex@test.com", 1982);
        if (type === 'client')
        return new Client ("John", "Smith", "john@test.com", 1976);
        if (type === 'guest')
        return new Guest ("Robert", "Merlo", "robert@test.com", 1999)

    }
    constructor(name, surname, email, yearOfBirth){
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.yearOfBirth = yearOfBirth;
    }
}

export class Admin extends User{
    read(){
        return (`I'm ${this.name}. I can read.`);
    };
    write(){
        return (`I'm ${this.name}. I can write.`);
    };
    update(){
        return (`I'm ${this.name}. I can update.`);
    };
    remove(){
        return (`I'm ${this.name}. I can remove.`);
    }
}



export class Moderator extends User{
    read(){
        return (`I'm ${this.name}. I can read.`);
    };
    write(){
        return (`I'm ${this.name}. I can write.`);
    };
    update(){
        return (`I'm ${this.name}. I can update.`);
    };
}

export class Client extends User{
    read(){
        return (`I'm ${this.name}. I can read.`);
    };
    write(){
        return (`I'm ${this.name}. I can write.`);
    };
} 

export class Guest extends User{
    read(){
        return (`I'm ${this.name}. I can read.`);
    };
}

const factory = new User();


const admin = factory.create('admin');
const moderator = factory.create('moderator');
const client = factory.create('client');
const guest = factory.create('guest');

admin.read();
admin.write();
admin.update();
admin.remove();

moderator.read();
moderator.write();
moderator.update();

client.read();
client.write();

guest.read();


