    export const User = function User(name, surname, email, yearOfBirth){
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.yearOfBirth = yearOfBirth;
    }
    User.prototype.getAge = function(){
    let result = new Date().getFullYear() - this.yearOfBirth
    return result
    };

    User.prototype.getFullname = function(){
        return (`${this.name} ${this.surname}`)
    };


    export const Admin = function Admin(name, surname, email, yearOfBirth){
    User.call(this, name, surname, email, yearOfBirth)
    }

    Admin.prototype = Object.create(User.prototype);

    Admin.prototype.read = function(){
    return (`I'm ${this.name}. I can read.`);
    }

    Admin.prototype.write = function(){
        return (`I'm ${this.name}. I can write.`);
    }

    Admin.prototype.update = function(){
    return (`I'm ${this.name}. I can update.`);
    }

    Admin.prototype.remove = function(){
    return (`I'm ${this.name}. I can remove.`);
    }


    export const Moderator = function Moderator(name, surname, email, yearOfBirth){
    User.call(this, name, surname, email, yearOfBirth)
    }
    
    Moderator.prototype = Object.create(User.prototype);

    Moderator.prototype.read = function(){
    return (`I'm ${this.name}. I can read.`);
    }

    Moderator.prototype.write = function(){
        return (`I'm ${this.name}. I can write.`);
    }

    Moderator.prototype.update = function(){
    return (`I'm ${this.name}. I can update.`);
    }

    export const Client = function Client(name, surname, email, yearOfBirth){
    User.call(this, name, surname, email, yearOfBirth)
    }
        
    Client.prototype = Object.create(User.prototype);
    
    Client.prototype.read = function(){
        return (`I'm ${this.name}. I can read.`);
    }
    
    Client.prototype.write = function(){
        return (`I'm ${this.name}. I can write.`);
    }


    export const Guest = function Guest(name, surname, email, yearOfBirth){
    User.call(this, name, surname, email, yearOfBirth)
    }
                
    Guest.prototype = Object.create(User.prototype);
            
    Guest.prototype.read = function(){
        return (`I'm ${this.name}. I can read.`);
    }


const admin = new Admin("Dmytro", "Yummy", "dmytro@test.com", 1995);
const moderator = new Moderator("Alex", "Morti", "alex@test.com", 1982);
const client = new Client("John", "Smith", "john@test.com", 1976);
const guest = new Guest("Robert", "Merlo", "robert@test.com", 1999);

admin.getAge();
admin.getFullname();
admin.read();
admin.write();
admin.update();
admin.remove();

moderator.getAge();
moderator.getFullname();
moderator.read();
moderator.write();
moderator.update();

client.getAge();
client.getFullname();
client.read();
client.write();

guest.getAge();
guest.getFullname();
guest.read();